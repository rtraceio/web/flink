namespace Flink.WebUI.Ports.IoC;

public static class ServiceCollectionExtensions {
  public static IServiceCollection RegisterWebUi(this IServiceCollection serviceCollection) {
    return serviceCollection;
  }
}
