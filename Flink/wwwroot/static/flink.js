function isURL(str) {
  const urlRegex = /(?:https?:\/\/)?(?:www\.)?[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}(:\d+)?(?:\/[^\s?#]*)*(\?[^\s#]*)?(#[^\s]*)?/;
  return new RegExp(urlRegex, 'i').test(str);
}

function setStatus(element, text) {
  element.innerHTML = text;
}

function setInputError(inputUrlElement, statusElement) {
  inputUrlElement.classList.add("error-glow");
  setStatus(statusElement, "❌ The entered URL is not valid");
}

function clearInputError(inputUrlElement, statusElement) {
  inputUrlElement.classList.remove("error-glow");
  setStatus(statusElement, "");
}
function copyURLToClipboard(statusElement, url) {
  console.debug("Clipboard")
  navigator.clipboard
    .writeText(url)
    .then(() => setStatus(statusElement, "📋 Copied to clipboard"))
    .catch(() => console.error('Could not write to clipboard.'));
}

function retryHttpGetCallUntil200(url, interval, callback) {
  const intervalId = setInterval(() => {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          clearInterval(intervalId);
          callback();
        }
        if (xhr.status !== 404) {
          clearInterval(intervalId);
        }
      }
    };
    xhr.send();
  }, interval);
}

(function() {
  const status = document.getElementById("status-text");
  const inputUrl = document.getElementById("input-url");
  const outputUrl = document.getElementById("output-url");
  const outputQrCode = document.getElementById("output-qr-code");
  const shortenAction = document.getElementById("shorten-action");
  const embedIframe = document.getElementById("embed-output");
  const embedLoader = document.getElementById("embed-loader");

  let inputDebounceTimer;
  let flinkifyButtonLocked;

  inputUrl.addEventListener('input', () => {
    flinkifyButtonLocked = false;
    clearTimeout(inputDebounceTimer);
    clearInputError(inputUrl, status);

    inputDebounceTimer = setTimeout(() => {
    isURL(inputUrl.value)
      ? clearInputError(inputUrl, status)
      : setInputError(inputUrl, status);
    }, 1000);
  });

  // Submit on Enter
  inputUrl.addEventListener('keyup', (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      shortenAction.click();
    }
  });

  outputUrl.addEventListener('dblclick', () => copyURLToClipboard(outputUrl.value));

  shortenAction.addEventListener('click', () => {
    if(flinkifyButtonLocked === true)
      return;

    flinkifyButtonLocked = true;
    // Hide the QR code
    outputQrCode.style.display = "none";
    outputQrCode.src = "";
    // Hide the output field
    outputUrl.style.display = "none";
    outputUrl.value = "";

    if(!isURL(inputUrl.value)) {
      setInputError(inputUrl, status);
      return;
    }

    const hasSchemaPrefix = [
      inputUrl.value.startsWith("http://"),
      inputUrl.value.startsWith("https://")
    ].reduce((acc, val) => acc || val, false);
    inputUrl.value = hasSchemaPrefix ? inputUrl.value : `https://${inputUrl.value}`;

    const r = new XMLHttpRequest();
    r.open('POST', '/to', true);
    r.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    r.onload = function() {
      if (r.status === 201) {
        clearInputError(inputUrl, status);
        embedIframe.style.display = 'none';
        const data = JSON.parse(r.responseText);
        console.log("Created ShortLink", data);
        // Display the shortened URL
        outputUrl.value = `${document.URL}${data['id']}`;
        outputUrl.style.display = "block";
        // Display the QR code
        outputQrCode.src = `${document.URL}qr/${data['id']}?height=202`;
        outputQrCode.style.display = "block";
        // set the iframe
        embedLoader.style.display = 'block';
        retryHttpGetCallUntil200(`${document.URL}meta/${data['id']}`, 300, () => {
          embedLoader.style.display = 'none';
          embedIframe.src = `${document.URL}embed/${data['id']}`;
          embedIframe.contentWindow.document.location.href=`${document.URL}embed/${data['id']}`;
          embedIframe.style.display = 'block';
        });

        setStatus(status, "✅ URL successfully shortened")
      }
    };

    r.send(JSON.stringify({
      "uri": inputUrl.value,
      "creator": "webui",
      "preferredIdLength": 4
    }));
  })
})();
